<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\FileManager;
use app\components\behaviors\ImageBehavior;
use app\components\behaviors\ViewCounterBehavior;

/**
 * This is the model class for table "hospital".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $address
 * @property string $image
 *
 * @property DoctorToHospital[] $doctorToHospitals
 */
class Hospital extends \yii\db\ActiveRecord
{
    const IMAGE_PATH = 'images/hospital/';
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hospital';
    }

    /**
     * @return array
     */
    public static function getList()
    {
        $hospitals = self::find()->asArray()->all();
        return ArrayHelper::map($hospitals, 'id', 'title');
    }

    public function behaviors()
    {
        return [
            ImageBehavior::class,
            ViewCounterBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'address'], 'string'],
            [['title', 'address'], 'required'],
            [['title', 'image'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png, jpg, jpeg, JPEG'],
            [['title'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'description' => 'Описание',
            'address' => 'Адрес',
            'image' => 'Изображение',
            'counter' => 'Просмотров',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoctorToHospitals()
    {
        return $this->hasMany(DoctorToHospital::class, ['hospital_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getDoctors()
    {
        return $this->hasMany(Doctor::class, ['id' => 'doctor_id'])->via('doctorToHospitals');
    }

    /**
     * @return integer
     */
    public function getDoctorsCount()
    {
        return $this->getDoctors()->count();
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return
     */
    public function beforeDelete()
    {
        $this->deleteImage($this->image);
        return parent::beforeDelete();
    }

}
