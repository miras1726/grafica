<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doctor_to_hospital".
 *
 * @property int $id
 * @property int $doctor_id
 * @property int $hospital_id
 *
 * @property Doctor $doctor
 * @property Hospital $hospital
 */
class DoctorToHospital extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctor_to_hospital';
    }

    /**
     * @return boolean
     */
    public static function isExists($doctorId, $hospitalId)
    {
        return self::find()->where(['doctor_id' => $doctorId, 'hospital_id' => $hospitalId])->exists();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['doctor_id', 'hospital_id'], 'required'],
            [['doctor_id', 'hospital_id'], 'integer'],
            [['doctor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Doctor::className(), 'targetAttribute' => ['doctor_id' => 'id']],
            [['hospital_id'], 'exist', 'skipOnError' => true, 'targetClass' => Hospital::className(), 'targetAttribute' => ['hospital_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'doctor_id' => 'Doctor ID',
            'hospital_id' => 'Hospital ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoctor()
    {
        return $this->hasOne(Doctor::class, ['id' => 'doctor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHospital()
    {
        return $this->hasOne(Hospital::class, ['id' => 'hospital_id']);
    }



}
