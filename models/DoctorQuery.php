<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 19.07.2019
 * Time: 0:40
 */

namespace app\models;

use yii\db\ActiveQuery;

class DoctorQuery extends ActiveQuery
{
    public function init()
    {
        parent::init();
    }

    public function withImage()
    {
        return $this->andOnCondition(['!=', 'image', '']);
    }

    public function name($name)
    {
        return $this->andOnCondition(['name' => $name]);
    }

    public function sort()
    {
        return $this->orderBy(['id' => SORT_DESC]);
    }

}