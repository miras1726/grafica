<?php

namespace app\models;

use Yii;
use app\models\DoctorToHospital;
use app\components\FileManager;
use app\components\behaviors\ImageBehavior;
use app\components\behaviors\ViewCounterBehavior;
use app\models\DoctorQuery;
/**
 * This is the model class for table "doctor".
 *
 * @property int $id
 * @property string $name
 * @property string $born
 * @property string $position
 * @property string $image
 *
 * @property DoctorToHospital[] $doctorToHospitals
 */
class Doctor extends \yii\db\ActiveRecord
{
    const IMAGE_PATH = 'images/doctor/';

    public $hospitals;

    public static function find()
    {
        return new DoctorQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'doctor';
    }

    public function behaviors()
    {
        return [
            ImageBehavior::class,
            ViewCounterBehavior::class,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'position', 'born'], 'required'],
            [['name', 'position', 'image'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png, jpg, jpeg, JPEG'],
            ['hospitals', 'each', 'rule' => ['integer']],
            [['born'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'ФИО',
            'born' => 'Дата рождения',
            'position' => 'Должность',
            'image' => 'Изображение',
            'hospitals' => 'Мед центры',
            'counter' => 'Просмотров',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoctorToHospitals()
    {
        return $this->hasMany(DoctorToHospital::class, ['doctor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveRecord[]
     */
    public function getHospitals()
    {
        return $this->hasMany(Hospital::class, ['id' => 'hospital_id'])->via('doctorToHospitals')->all();
    }

    /**
     * @param $hospitals mixed
     * @return mixed
     */
    public function setHospitals($hospitals)
    {
        if (count($hospitals) && is_array($hospitals)) {
            foreach ($hospitals as $key => $value) {
                if (DoctorToHospital::isExists($this->id, $value)) continue;
                $model = new DoctorToHospital();
                $model->doctor_id = $this->id;
                $model->hospital_id = $value;
                $model->save();
            }
        }
        return true;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return
     */
    public function beforeDelete()
    {
        $this->deleteImage($this->image);
        return parent::beforeDelete();
    }

}
