<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>
<?php if ($model->getHospitals()):?>
    <?php foreach ($model->getHospitals() as $item):?>
        <span class="d-inline-block mr-3"><?php echo $item->title?>
            <?= Html::a('X', ['delete-hospital-relation', 'doctorId' =>$model->id, 'hospitalId' => $item->id], [
                'class' => 'btn btn-danger btn-sm',
                'data' => [
                    'confirm' => 'Вы уверены?',
                    'method' => 'post',
                ],
            ]) ?>
            </span>
    <?php endforeach;?>
    <hr>
<?php endif;?>