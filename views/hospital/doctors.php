<?php

use yii\helpers\Html;
use yii\widgets\LinkPager;
/* @var $doctors app\models\Doctor */
/* @var $hospital app\models\Hospital */
?>
<h1>Врачи - <?php echo $hospital->title; ?></h1>
<table class="table">
    <thead>
    <tr>
        <th scope="col">ID</th>
        <th scope="col">ФИО</th>
        <th scope="col">Должность</th>
        <th scope="col">Дата рождения</th>
        <th scope="col">Изображение</th>
    </tr>
    </thead>
    <tbody>
    <?php if ($doctors) :?>
        <?php foreach ($doctors as $doctor) :?>
            <tr>
                <th scope="row"><?php echo $doctor->id; ?></th>
                <td><?php echo $doctor->name; ?></td>
                <td><?php echo $doctor->position; ?></td>
                <td><?php echo $doctor->born; ?></td>
                <td><?php echo Html::img('/'.$doctor::IMAGE_PATH.$doctor->image, ['width' => 50]); ?></td>
            </tr>
        <?php endforeach;?>
    <?php endif;?>

    </tbody>
</table>

<div class="col-12">
    <hr class="separator-transparent">
    <nav aria-label="navigation">
        <?=LinkPager::widget([
            'pagination' => $pagination
        ]);?>
    </nav>
</div>
