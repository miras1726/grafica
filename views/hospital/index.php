<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\HospitalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мед центры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hospital-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'title',
                'format' => 'raw',
                'value' => function ($model){
                    return Html::a($model->title, ['doctors', 'id' => $model->id]);
                },
            ],
            'description:ntext',
            'address:ntext',
            [
                'attribute' => 'image',
                'format' => 'raw',
                'value' => function ($model){
                    return Html::img($model->getImage(), ['width' => 100]);
                },
            ],
            [
                'attribute' => 'Врачей',
                'value' => function ($model){
                    return $model->getDoctorsCount();
                },
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
