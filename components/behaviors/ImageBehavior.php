<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 18.07.2019
 * Time: 22:17
 */

namespace app\components\behaviors;

use Yii;
use yii\base\Behavior;
use app\components\FileManager;

class ImageBehavior extends Behavior
{
    /**
     * @return string
     */
    public function setImage($image, $oldImage = false)
    {
        if ($oldImage) $this->deleteImage($oldImage);
        return FileManager::upload($this->owner, $image, $this->owner::IMAGE_PATH, 'image');
    }

    /**
     * @return string
     */
    public function getImage()
    {
        $image = $this->owner->image;
        if ($image != '' && !is_null($image)) {
            return '/' . $this->owner::IMAGE_PATH . $image;
        }
    }

    /**
     * @return boolean
     */
    public function deleteImage($image)
    {
        return FileManager::delete($this->owner::IMAGE_PATH, $image);
    }
}