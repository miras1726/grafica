<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 18.07.2019
 * Time: 22:45
 */

namespace app\components\behaviors;

use yii\base\Behavior;

class ViewCounterBehavior extends Behavior
{
    /**
     * Intrements counter field of the owner model
     */
    public function incrementCounter()
    {
        $this->owner->updateCounters(['counter' => 1]);
    }

    /**
     * Resets counter field of the owner model
     */
    public function resetCounter()
    {
        $this->owner->counter = 0;
        $this->owner->save();
    }
}