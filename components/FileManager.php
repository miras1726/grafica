<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 15.07.2019
 * Time: 22:02
 */

namespace app\components;

use yii\db\ActiveRecord;
use yii\web\UploadedFile;

class FileManager
{
    /**
     * Upload the given file
     * @param ActiveRecord $model
     * @param mixed $image
     * @param string $path
     * @param string $attribute
     * @return string
     */
    public static function upload($model, $image, $path = 'images/', $attribute)
    {
        if ($image == null) return;
        $image = UploadedFile::getInstance($model, $attribute);
        $newFileName = self::generateName() . '.' . $image->extension;
        if ($image->saveAs($path . $newFileName)) {
            return $newFileName;
        } else {
            return false;
        }
    }

    /**
     * Generate filename
     * @return string
     */
    public static function generateName()
    {
        return md5(microtime());
    }

    /**
     * Delete the file if exists
     * @param string $path
     * @param string $fileName
     * @return boolean
     */
    public static function delete($path, $fileName)
    {
        if (!$fileName) return false;
        $file = $path . $fileName;
        if (file_exists($file) && $fileName != '') {
            return (unlink($file)) ? true : false;
        }
    }

}