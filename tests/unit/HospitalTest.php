<?php

use app\tests\fixtures\HospitalFixture;
use app\models\Hospital;

class HospitalTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _fixtures()
    {
        return ['hospitals' => HospitalFixture::class];
    }

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testGetTitle()
    {
        $hospital = $this->tester->grabFixture('hospitals', 'hospital0');

        expect($hospital->getTitle())->equals('Lindgren, Wilkinson and Walker');
    }

    public function testIsImagePathCorrect()
    {
        expect(Hospital::IMAGE_PATH)->equals('images/hospital/');
    }

    public function testDoctorsCount()
    {
        $hospital = $this->tester->grabFixture('hospitals', 'hospital1');

        expect($hospital->getDoctorsCount())->equals(0);
    }

}