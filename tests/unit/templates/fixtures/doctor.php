<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */
return [
    'name' => $faker->name,
    'born' => $faker->date($format = 'Y-m-d', $max = 'now'),
    'position' => $faker->jobTitle,
    'image' => '',
];