<?php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */
return [
    'title' => $faker->company,
    'description' => $faker->text($maxNbChars = 200),
    'address' => $faker->address,
    'image' => '',
];