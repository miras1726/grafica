<?php

use app\tests\fixtures\DoctorFixture;
use app\models\Doctor;

class DoctorTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _fixtures()
    {
        return ['doctors' => DoctorFixture::class];
    }

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testGetName()
    {
        $doctor = $this->tester->grabFixture('doctors', 'doctor0');

        expect($doctor->name)->equals('Alysson Heaney');
    }

    public function testIsImagePathCorrect()
    {
        expect(Doctor::IMAGE_PATH)->equals('images/doctor/');
    }
}