<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 16.07.2019
 * Time: 20:53
 */

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class DoctorFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Doctor';
}