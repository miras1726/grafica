<?php

return [
    'doctor0' => [
        'name' => 'Alysson Heaney',
        'born' => '1993-12-21',
        'position' => 'Pediatricians',
        'image' => '',
    ],
    'doctor1' => [
        'name' => 'Miss Rosalind Wilkinson',
        'born' => '1994-03-18',
        'position' => 'Pharmaceutical Sales Representative',
        'image' => '',
    ],
    'doctor2' => [
        'name' => 'Nathen Hudson',
        'born' => '1996-04-21',
        'position' => 'Conservation Scientist',
        'image' => '',
    ],
    'doctor3' => [
        'name' => 'Miss Beaulah Schulist I',
        'born' => '2015-02-02',
        'position' => 'Museum Conservator',
        'image' => '',
    ],
    'doctor4' => [
        'name' => 'Ms. Gerry Boyer',
        'born' => '2015-08-23',
        'position' => 'Occupational Therapist Assistant',
        'image' => '',
    ],
    'doctor5' => [
        'name' => 'Shemar Dicki',
        'born' => '1974-03-23',
        'position' => 'Municipal Fire Fighter',
        'image' => '',
    ],
    'doctor6' => [
        'name' => 'Myah Harris',
        'born' => '1970-01-30',
        'position' => 'Garment',
        'image' => '',
    ],
    'doctor7' => [
        'name' => 'Mrs. Joyce Will',
        'born' => '2005-08-12',
        'position' => 'Welfare Eligibility Clerk',
        'image' => '',
    ],
    'doctor8' => [
        'name' => 'Carmel Hagenes',
        'born' => '1992-05-30',
        'position' => 'Spotters',
        'image' => '',
    ],
    'doctor9' => [
        'name' => 'Ms. Natalia Bergstrom',
        'born' => '1986-02-23',
        'position' => 'HR Specialist',
        'image' => '',
    ],
    'doctor10' => [
        'name' => 'Irving Crist',
        'born' => '2013-11-02',
        'position' => 'Computer Science Teacher',
        'image' => '',
    ],
    'doctor11' => [
        'name' => 'Jerod Shields',
        'born' => '2006-10-03',
        'position' => 'Agricultural Engineer',
        'image' => '',
    ],
    'doctor12' => [
        'name' => 'Serena Davis',
        'born' => '1995-07-27',
        'position' => 'Algorithm Developer',
        'image' => '',
    ],
    'doctor13' => [
        'name' => 'Prof. Chadrick Price III',
        'born' => '2011-09-27',
        'position' => 'Anthropologist',
        'image' => '',
    ],
    'doctor14' => [
        'name' => 'Johnny Howell',
        'born' => '1977-01-02',
        'position' => 'Printing Machine Operator',
        'image' => '',
    ],
];
