<?php

return [
    'hospital0' => [
        'title' => 'Lindgren, Wilkinson and Walker',
        'description' => 'Aut ut est nihil placeat nisi. Et exercitationem iusto magnam autem. Sunt expedita blanditiis dolorem et vel consequatur incidunt.',
        'address' => '31202 Mayer Causeway
Lyricview, VA 48080-5740',
        'image' => '',
    ],
    'hospital1' => [
        'title' => 'Howell, Gottlieb and Hegmann',
        'description' => 'Omnis cumque amet fuga et dolorem et doloremque. Facilis temporibus asperiores praesentium velit. Animi aut modi id maxime neque. Saepe voluptatibus corporis amet inventore est asperiores.',
        'address' => '42094 Mueller Grove
East Nya, AL 56727-8875',
        'image' => '',
    ],
    'hospital2' => [
        'title' => 'Mraz and Sons',
        'description' => 'Neque numquam at architecto velit odio aliquid. Sint eligendi doloribus excepturi voluptate. Mollitia alias odit cupiditate est dolor. Iste sunt modi sit.',
        'address' => '51675 Rodrick Flats
West Abbigailmouth, VA 24463',
        'image' => '',
    ],
    'hospital3' => [
        'title' => 'Macejkovic, O\'Kon and Mayert',
        'description' => 'Quae quisquam maiores magni neque asperiores vel qui. Qui aspernatur et et aut modi adipisci quam sed. Nostrum omnis eum omnis saepe libero.',
        'address' => '102 Gorczany Extension
Schaeferville, WY 24889-5992',
        'image' => '',
    ],
    'hospital4' => [
        'title' => 'Schmidt LLC',
        'description' => 'Repudiandae sunt doloribus pariatur. Cumque provident numquam aperiam sunt recusandae doloribus. Temporibus architecto voluptatem nulla illo. Officiis voluptate repellendus sed eius.',
        'address' => '5347 Willms Hills Apt. 428
Nellafort, ID 14503',
        'image' => '',
    ],
    'hospital5' => [
        'title' => 'Abshire-Becker',
        'description' => 'Omnis odit distinctio in. In possimus qui et alias. Enim est qui beatae ab. Consequatur eos sunt eaque adipisci aut. Ad accusamus dicta vel quia.',
        'address' => '51544 Felix Forges
Schinnermouth, ME 47829-5082',
        'image' => '',
    ],
    'hospital6' => [
        'title' => 'Swift, Schimmel and Adams',
        'description' => 'Fuga officia vel et atque. Vero laborum voluptas inventore voluptatem. Magnam id doloribus et porro accusamus voluptatem ut temporibus. Sapiente consequatur consequatur voluptatem est dolorum cumque.',
        'address' => '4940 Willms Bridge Suite 177
East Grover, MT 57047',
        'image' => '',
    ],
    'hospital7' => [
        'title' => 'Walter and Sons',
        'description' => 'Dolorem dolor id aliquid ex voluptatibus ut nihil. Quia veniam inventore ipsa assumenda quia dicta eum ea. Fuga omnis quia dolorum perferendis voluptatem.',
        'address' => '3716 Wunsch Circle Apt. 913
Alfredoview, VA 06560',
        'image' => '',
    ],
    'hospital8' => [
        'title' => 'Davis Group',
        'description' => 'Cupiditate odio fugiat sunt impedit quam. Repellendus quaerat nulla nam est qui sed. Sint quisquam et reprehenderit consectetur. Vel sed eius aut eos molestias et nisi praesentium.',
        'address' => '159 Boyd Isle
Myriamberg, DE 67550-6571',
        'image' => '',
    ],
    'hospital9' => [
        'title' => 'Rodriguez, Morar and Leuschke',
        'description' => 'Ut voluptas consequuntur ab sit aliquam. Et excepturi et veniam eaque est possimus. Voluptas cumque ea et soluta. Et voluptas quas aut qui exercitationem ut reiciendis ullam. Omnis beatae quo autem.',
        'address' => '68185 Tillman Burg Apt. 056
Zboncakhaven, AR 31717',
        'image' => '',
    ],
];
