<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 16.07.2019
 * Time: 18:51
 */

namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class HospitalFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Hospital';
    public $dataFile = __DIR__ . '/test/fixtures/data/hospital.php';
}