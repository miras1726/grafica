<?php

namespace app\controllers;

use Yii;
use app\models\Doctor;
use app\models\Hospital;
use app\models\DoctorToHospital;
use app\models\DoctorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * DoctorController implements the CRUD actions for Doctor model.
 */
class DoctorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                    'delete-hospital-relation' => ['POST'],
                    'reset-counter' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Doctor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DoctorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Doctor model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model->incrementCounter();
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Doctor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Doctor();

        if ($model->load(Yii::$app->request->post())) {

            $image = UploadedFile::getInstance($model, 'image');

            if ($image) {
                $model->image = $model->setImage($image);
            }

            if ($model->save()) {
                $model->setHospitals($model->hospitals);
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        $hospitalList = Hospital::getList();

        return $this->render('create', [
            'model' => $model,
            'hospitalList' => $hospitalList,
        ]);
    }

    /**
     * Updates an existing Doctor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldImage = $model->image;

        if ($model->load(Yii::$app->request->post())) {
            $image = UploadedFile::getInstance($model, 'image');

            if ($image) {
                $model->image = $model->setImage($image, $oldImage);
            } else {
                $model->image = $oldImage;
            }

            $model->update();
            $model->setHospitals($model->hospitals);
            return $this->redirect(['view', 'id' => $model->id]);
        }
        $hospitalList = Hospital::getList();

        return $this->render('update', [
            'model' => $model,
            'hospitalList' => $hospitalList,
        ]);
    }

    /**
     * Deletes an existing Doctor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Doctor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Doctor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Doctor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Deletes an existing DoctorToHospital record.
     * @param integer $doctorId
     * @param integer $hospitalId
     * @return Response
     */
    public function actionDeleteHospitalRelation($doctorId, $hospitalId)
    {
        $relation = DoctorToHospital::find()->where(['doctor_id' => $doctorId, 'hospital_id' => $hospitalId])->one();

        if ($relation) {
            $relation->delete();
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Resets counter of Hospital model.
     * @param integer $id
     * @return \yii\web\Response
     */
    public function actionResetCounter($id)
    {
        $model = $this->findModel($id);
        $model->resetCounter();
        return $this->redirect(Yii::$app->request->referrer);
    }

}
