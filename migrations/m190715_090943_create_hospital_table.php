<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%hospital}}`.
 */
class m190715_090943_create_hospital_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%hospital}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->unique(),
            'description' => $this->text(),
            'address' => $this->text(),
            'image' => $this->string(255),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%hospital}}');
    }
}
