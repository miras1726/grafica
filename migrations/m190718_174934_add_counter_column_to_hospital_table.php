<?php

use yii\db\Migration;

/**
 * Handles adding counter to table `{{%hospital}}`.
 */
class m190718_174934_add_counter_column_to_hospital_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('hospital', 'counter', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('hospital', 'counter');
    }
}
