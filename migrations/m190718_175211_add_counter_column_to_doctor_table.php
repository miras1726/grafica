<?php

use yii\db\Migration;

/**
 * Handles adding counter to table `{{%doctor}}`.
 */
class m190718_175211_add_counter_column_to_doctor_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('doctor', 'counter', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('doctor', 'counter');
    }
}
