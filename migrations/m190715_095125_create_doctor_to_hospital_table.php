<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%doctor_to_hospital}}`.
 */
class m190715_095125_create_doctor_to_hospital_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%doctor_to_hospital}}', [
            'id' => $this->primaryKey(),
            'doctor_id' => $this->integer()->notNull(),
            'hospital_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'doctor_foreign_key',
            'doctor_to_hospital',
            'doctor_id',
            'doctor',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'hospital_foreign_key',
            'doctor_to_hospital',
            'hospital_id',
            'hospital',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'hospital_foreign_key',
            'doctor_to_hospital'
        );
        $this->dropForeignKey(
            'doctor_foreign_key',
            'doctor_to_hospital'
        );
        $this->dropTable('{{%doctor_to_hospital}}');
    }
}
